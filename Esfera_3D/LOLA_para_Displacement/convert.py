import numpy as np
import cv2

img = cv2.imread('../LOLA_map.jpg',3)
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

dim = hsv.shape
width = dim[0]
height = dim[1]

minimum = 999999
maximum = 0
for x in range(width):
    for y in range(height):
        hsv[x][y][0] = 255-hsv[x][y][0]
        hsv[x][y][1] = hsv[x][y][0]
        hsv[x][y][2] = hsv[x][y][0]
        minimum = min(minimum, hsv[x][y][0])
        maximum = max(maximum, hsv[x][y][0])

print(minimum)
print(maximum)

cv2.imshow('image',hsv)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.imwrite('LOLA_height.jpg', hsv)

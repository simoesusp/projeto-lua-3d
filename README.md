# Projeto Lua 3D

Projeto para planejarmos a construção da nossa Lua 3D

Esse projeto é complentar ao desenvolvimento do nosso Telescópio: https://gitlab.com/simoesusp/ProjetoTelescopio

## Resumo

- Queremos usar esse projeto para integrar toda uma comunidade Maker Paulista e Nacional!! 
- Temos muitas questões para resolver, como tentar extrair as informações de vários sites... 
- Depois processar os dados e convertê-los para formar um globo.... 
- Depois particionar em várias placas que se encaixem... 
- Depois coordenar um esforço coletivo de impressão e montagem (pretendo replicar a prototipação em pelo menos 3 grupos, para trocar experiências e soluções...) ... 
- E por fim, documentar todo o processo para que possa ser reproduzido por Universidades, escolas e espaços maker em todo o Brasil. Minha idéia é entregar tipo um kit, e buscar financiamento na FAPESP (PIPE) para transferir esse knowhow para startups produzirem para venda às escolas particulares e doação para escolas públicas!!

## Metas

- Primeira Meta (Accomplished): a construção de uma Luasinha de 20cm de diâmetro! 
  - Com isso, podemos atrair mais pessoas e até financiamento para os gastos com matéria prima de impressão!
- Segunda Meta: adaptar o protótiopo para um diâmetro de 2 metros!
  - Teremos que subdividir a superfície da esfera em várias plaquinhas de 10cm x 10cm e bolar uma maneira de encaichá-las umas nas outras na ordem certa...
- Terceira Meta: planejamento e construção de uma história interativa para acompanhar e introduzir a Lua 3d.
  - Inicialmente, a história será criada na ferramenta Scratch, integrando a Lua e o ensino da lógica de programação por meio da customização dos personagens e cenários.
  - Definir manuais de ensino para as seguintes áreas:
    - escrita e construção de histórias;
    - customizar e ilustrar personagens em arte digital;
    - programação em blocos no Scratch para a criação das histórias.
  - Oficina de Scratch para o desenvolvimento da história inicial:
    - história piloto para teste.


## Reuniões
- dia 21/04 - https://drive.google.com/file/d/1kmhOAHHzmwJv63Q1kc84tHqAObRQIxLN/view?usp=sharing
  - discussão de estratégias de ação
- dia 22/04 Primeira partte - https://drive.google.com/file/d/1dB48bYdGHlj1dF5ekoRIV3PgX9V2WnrH/view?usp=sharing
  - download do arquivo de imagem com elevações
  - Software para converter o arquivo em matriz de elevação
  - Uso do Blender para criar uma esfera e aplicar informação de elevação
  - Desenho de uma "casca" esférica com o relevo das crateras da lua
- dia 22/04 Segunda parte - https://drive.google.com/file/d/1xLawpMpFM2oZAQFmAcou0nncJDQUJ4zZ/view?usp=sharing
  - Preparação do arquivo do Blender para impressão
  - impressão de meia lua

# O Projeto

Estamos começando um projeto de imprimir uma LUA na impressora 3d de uns 2m de diâmetro! Com as crateras e montanhas precisamente mapeadas!! Tem esse site (https://quickmap.lroc.asu.edu/ ) que organisa imagens de diversos sensores da lua, obtidas de uma sonda orbital LROC. São imagens belíssimas de brincar! E também tem o https://www.google.com/maps/space/moon/ que usamos em aulas de tecnologia com as crianças, no projeto Principia - Robôs na Escola. A ideia é extrair as informações de elevação, e converter para várias placas impressas na impressora 3D, que depois seriam coladas em uma bola de isopor bem grande!!! Dai´ doariamos para a APAE para que crianças cegas possam ter uma ideia das crateras e montanhas da Lua!! Eu quero fazer tipo um quebra cabeças.... Pras crianças terem que olhar no telescópio e ir montando na bola elas mesmas, conforme forem identificando os detalhes... 
Nessas fotos, a primeira foi com nosso telescópio... Mas com as imagens do Lunar Reconnaissance Orbiter (https://g.co/kgs/29VqKb) dá pra ver inclusive a base do módulo lunar da Apolo 15 e até as pegadas dos astronautas na Lua!!
Pasta do drive com os arquivos blender da lua: https://drive.google.com/drive/folders/1_HqyiF0d5MGMmgBDd7z9pb72yptkxUQj?usp=sharing

<div align="center">
 <img src="./images/Lua_Apolo15.jpg" height="300">
</div>
<div align="center">
 <img src="./images/Lua1.jpg" width="300">
 <img src="./images/Lua2.jpg" width="300">
 <img src="./images/Lua3.jpg" width="300">
</div>



